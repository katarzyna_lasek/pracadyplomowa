import {images} from '../constants';

export const button3 = [
  {
    id: 1,
    name: '8 stopni jogi',
    icon: images.yogaphoto4,
    time: '5min',
    link: 'YogaLevel',
  },
  {
    id: 2,
    name: 'Liczenie w sanskrycie',
    icon: images.yogaphoto5,
    time: '1h 15min',
    link: 'Counting',
  },
  {
    id: 3,
    name: 'Lista styli',
    icon: images.yogaphoto6,
    time: '1h 30min',
    link: 'YogaStyle',
  },
];
