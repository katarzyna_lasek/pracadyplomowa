import {icons, images} from '../constants';

export const button2 = [
  {
    id: 1,
    name: 'Jogasutry',
    icon: images.yogaphoto2,
    time: '5min',
    link: 'Jogasutry',
  },
  {
    id: 2,
    name: 'Mantry',
    icon: images.yogaphoto3,
    time: '7min',
    link: 'Mantra',
  },
];
