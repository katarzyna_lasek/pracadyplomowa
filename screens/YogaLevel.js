import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons, images} from '../constants';

const YogaLevel = ({navigation}) => {
  const YogaLevelData = [
    {
      id: 1,
      nameSa: 'Jama',
      namePl: 'zasady życia społecznego',
      description:
        '–ahimsa – niekrzywdzenie\n' +
        '–satja – prawda,\n' +
        '–aszteja– powstrzymanie się od kradzieży,\n' +
        '–brahmaczaria – wstrzemięźliwość,\n' +
        '–aparigraha– nie pragnienie, brak zachłanności.',
    },
    {
      id: 2,
      nameSa: 'Nijama',
      namePl: 'zasady życia indywidualnego',
      description:
        '–santosza – zadowolenie,\n' +
        '–saucza – czystość,\n' +
        '–tapas – zapał,\n' +
        '–swadhjaja – kształcenie jaźni,\n' +
        '–Iśwara-pranidhana – poświęcenie.',
    },
    {
      id: 3,
      nameSa: 'Asana',
      namePl: 'opanowanie ciała',
      description: '',
    },
    {
      id: 4,
      nameSa: 'Pranajama',
      namePl: 'opanowanie oddechu',
      description: '',
    },
    {
      id: 5,
      nameSa: 'Pratjahara',
      namePl: 'powściągnięcie zmysłów',
      description: '',
    },
    {
      id: 6,
      nameSa: 'Dharana',
      namePl: 'koncentracja',
      description: '',
    },
    {
      id: 7,
      nameSa: 'Dhjana',
      namePl: 'medytacja',
      description: '',
    },
    {
      id: 8,
      nameSa: 'Samadhi',
      namePl: 'jedność',
      description: '',
    },
  ];
  const [yogaLevelData, setYogaLevelData] = React.useState(YogaLevelData);

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              8 stopni jogi
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderButtonAsanas() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: SIZES.padding,
        }}>
        <View style={{flexDirection: 'row', height: 150}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => navigation.navigate('AsanasList')}>
            <ImageBackground
              source={images.yogaphoto1}
              resizeMode="cover"
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.secondary,
                  fontSize: 35,
                  lineHeight: 50,
                  textAlign: 'center',
                  backgroundColor: COLORS.primary,
                }}>
                stopnie jogi
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderPranayamaSection(yogaLevelData) {
    const renderItem = ({item, index}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('ItemDetail', {
                item: item,
              })
            }>
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.h2,
                    color: COLORS.black,
                  }}>
                  {item.id} {item.nameSa}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: SIZES.radius}}>
                <Text style={{...FONTS.h3, color: COLORS.lightGray}}>
                  {item.namePl}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, margin: SIZES.padding}}>
          <FlatList
            data={yogaLevelData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView style={{marginTop: SIZES.radius}}>
        {renderButtonAsanas()}
        <View>{renderPranayamaSection(yogaLevelData)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default YogaLevel;
