import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons, images} from '../constants';

const Home = ({navigation}) => {
  const mantraData = [
    {
      id: 1,
      mantraSanscrit:
        'वन्दे गुरुणां चरणारबिन्डे सन्दर्शितस्वात्मसुखावबोधे\n' +
        'निःश्रेयसे जाङ्गलिकायमाने संसारहालाहलमोहशान्त्यै\n' +
        'आबाहुपुरुषाकारं शङ्खचक्रासिधारिणम्\n' +
        'सहस्रशिरसं श्वेतं म्रणमामि पतञ्जलिम्\n' +
        '\n',
      mantraLatin:
        'vande gurūṇām caraṇāravinde sandarśita-svātma-sukhāvabodhe\n' +
        'niḥśreyase jāṅgalikāyamāne saṃsāra-hālāhala-mohaśāntyai\n' +
        'ābāhu-puruṣākāram śaṅkha-cakrāsi-dhāriṇam\n' +
        'sahasra-śirasaṃ śvetaṃ praṇamāmi patañjalim\n' +
        '\n',
      mantraPolish:
        'Kłaniam się stopom guru lotosowi podobnym,\n' +
        'Dzięki którym odkryłem czym jest szczęście mej duszy.\n' +
        'Mej ostatecznej ostoi, działającemu jak lekarz,\n' +
        'By rozwiać złudzenia płynące z trucizny cyklu wcieleń.\n' +
        'Który do ramion ludzką ma postać,\n' +
        'Dzierży muszlę, dysk i miecz.\n' +
        'Z tysiącem bielą jaśniejących głów,\n' +
        'Pokłon składam Patañjalemu.\n',
    },
    {
      id: 2,
      mantraSanscrit:
        'स्वस्तिप्रजाभ्यः परिपालयन्तां न्यायेन मार्गेण महीं महीशाः\n' +
        'गोब्राह्मणेभ्यः शुभमस्तु नित्यं लोकाः समस्ताः सुखिनो भवन्तु\n' +
        'ॐ शान्तिः शान्तिः शान्तिः\n' +
        '\n',
      mantraLatin:
        'svasti prajabhyah paripalayantam\n' +
        'nyayena margena mahim mahiśah\n' +
        'gobrahmanebhyah subhamastu nityam\n' +
        'lokasamasta sukhino bhavantu\n' +
        'Om shanti shanti shanti\n' +
        '\n',
      mantraPolish:
        'Niech ludzkość będzie w wolności i dobrobycie.\n' +
        'Niech przywódcy ochraniają  ją  i utrzymują na właściwej ścieżce.\n' +
        'Niech pomyślność będzie z tymi którzy wiedzą że Ziemia jest święta.\n' +
        'Niech ludzkość będzie zjednoczona i szczęśliwa.\n' +
        'Om\n' +
        'Pokój. Pokój. Pokój\n',
    },
  ];
  const [mantra, setMantraData] = React.useState(mantraData);

  function renderHeader(profile) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              MANTRY
            </Text>
          </View>
        </View>
      </View>
    );
  }
  function renderMantraSection(mantra) {
    const renderItem = ({item, index}) => {
      return (
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'column',
              paddingVertical: 5,
              backgroundColor: 'rgba(0,0,0,0.3)',
              marginVertical: SIZES.base,
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{...FONTS.h4, color: COLORS.black}}>
                {item.mantraSanscrit}
              </Text>
            </View>
            <View
              style={{
                flex: 2,
                paddingHorizontal: SIZES.radius,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  paddingRight: SIZES.padding,
                  ...FONTS.h3,
                  color: COLORS.lightGray,
                }}>
                {item.mantraLatin}
              </Text>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{...FONTS.h4, color: COLORS.black}}>
                {item.mantraPolish}
              </Text>
            </View>
          </View>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            marginLeft: SIZES.padding,
            marginRight: SIZES.padding,
          }}>
          <FlatList
            data={mantraData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView>
        <View>{renderMantraSection(mantra)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
