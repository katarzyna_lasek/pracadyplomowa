import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';

import {COLORS, FONTS, SIZES, images} from '../constants';

const Jogasutry = ({navigation}) => {
  const YogasutraData = [
    {
      id: 1,
      number: '1.1',
      nameSa: 'Atha yoganusasanam',
      namePl: 'Oto zaczyna się nauka jogi',
    },
    {
      id: 2,
      number: '1.2',
      nameSa: 'Yogas citta-vrtti-nirodhah',
      namePl: 'Joga powstrzymuje zmiany stanów umysłu',
    },
    {
      id: 3,
      number: '1.3',
      nameSa: 'Tada drastuh svarupe vasthanam',
      namePl: 'Wtedy obserwujący trwa nieporuszony w swym naturalnym stanie',
    },
    {
      id: 4,
      number: '1.4',
      nameSa: 'Vrtti sarupyam itarata',
      namePl: 'W przeciwnym wypadku trwać będzie stan zmiennego umysłu',
    },
  ];
  const [yogasutra, setYogasutra] = React.useState(YogasutraData);

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              JOGASUTRY
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderButton() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: SIZES.padding,
        }}>
        <View style={{flexDirection: 'row', height: 100}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => navigation.navigate('AsanasList')}>
            <ImageBackground
              source={images.yogaphoto1}
              resizeMode="cover"
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.secondary,
                  fontSize: 35,
                  lineHeight: 50,
                  textAlign: 'center',
                  backgroundColor: COLORS.primary,
                }}>
                Historia jogasutr
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderYogasutraSection(yogasutra) {
    const renderItem = ({item, index}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('ItemDetail', {
                item: item,
              })
            }>
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.body2,
                    color: COLORS.black,
                  }}>
                  {item.number} {item.namePl}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={{...FONTS.body3, color: COLORS.lightGray}}>
                  {item.nameSa}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, marginLeft: SIZES.padding}}>
          <Text style={{...FONTS.h2}}>Lista Jogasutr:</Text>
          <FlatList
            data={yogasutra}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView>
        {renderButton()}
        <View>{renderYogasutraSection(yogasutra)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Jogasutry;
