import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';

import {COLORS, FONTS, SIZES} from '../constants';

const LineDivider = () => {
  return (
    <View style={{width: 1, paddingVertical: 5}}>
      <View
        style={{
          flex: 1,
          borderLeftColor: COLORS.gray,
          borderLeftWidth: 1,
        }}
      />
    </View>
  );
};

const Counting = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getMovies = async () => {
    try {
      const response = await fetch('http://192.168.56.1:3000/sanskritnumbers');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getMovies();
  }, []);

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              LICZENIE W SANSKRYCIE
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderNumbers() {
    const renderItem = ({item, index}) => {
      return (
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              paddingVertical: 5,
              marginVertical: SIZES.base,
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{...FONTS.h4, color: COLORS.black}}>{item.id}</Text>
            </View>
            <LineDivider />
            <View
              style={{
                flex: 2,
                alignItems: 'center',
              }}>
              <Text
                style={{
                  ...FONTS.h3,
                  color: COLORS.lightGray,
                }}>
                {item.sanskritNumber}
              </Text>
            </View>
          </View>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 1,
          }}>
          <FlatList
            data={data}
            keyExtractor={({id}, index) => id}
            renderItem={renderItem}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <ScrollView>
          <View>{renderNumbers()}</View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default Counting;
