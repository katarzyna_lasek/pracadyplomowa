import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Animated,
  FlatList,
} from 'react-native';
import {FONTS, COLORS, SIZES, icons} from '../constants';

const AsanaDetail = ({route, navigation}) => {
  React.useEffect(() => {
    let {item} = route.params;
    setItem(item);
  }, [item, route.params]);

  const [item, setItem] = React.useState(null);

  const description = {
    text:
      'stań w tadasanie\n' +
      'przejdź do rozkroku\n' +
      'skręć ramiona na zewnątrz i podnosząc bokiem wyciągnij w górę\n' +
      'następnie skręć lewą stopę mocno do wewnątrz a prawą 90′ na zewnątrz\n' +
      'dalej skręć w prawo biodra i tułów\n' +
      'z wdechem wyciągnij się mocno w górę połącz dłonie i odchyl głowę w tył\n' +
      'z wydechem zegnij prawą nogę do kąta prostego\n' +
      'podnosząc głowę wyprostuj prawą nogę i skręć się do centrum\n' +
      'wykonaj pozycję w drugą stronę.',
  };

  const benefits = {
    text: 2,
  };

  const contraindications = {
    text: 3,
  };

  const categoriesData = [
    {
      id: 1,
      categoryName: 'Instrukcja wykonania',
      items: [description],
    },
    {
      id: 2,
      categoryName: 'Działanie',
      items: [benefits],
    },
    {
      id: 4,
      categoryName: 'Przeciwwskazania',
      items: [contraindications],
    },
  ];

  const [categories, setCategories] = React.useState(categoriesData);
  const [selectedCategory, setSelectedCategory] = React.useState(1);
  const [scrollViewWholeHeight, setScrollViewWholeHeight] = React.useState(1);
  const [scrollViewVisibleHeight, setScrollViewVisibleHeight] =
    React.useState(0);

  const indicator = new Animated.Value(0);

  function renderAsanaInfoSection() {
    return (
      <View style={{flex: 1, position: 'relative'}}>
        <View
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundColor: COLORS.tertiary,
          }}
        />
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{...FONTS.h2, color: COLORS.background, marginTop: 10}}>
            {item.namePl}
          </Text>
          <Image
            source={item.image}
            resizeMode="cover"
            style={{width: 150, height: 150, marginTop: 10}}
          />
        </View>
      </View>
    );
  }

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              {item.nameS}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderDrishti() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            backgroundColor: 'rgba(0,0,0,0.3)',
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{...FONTS.h3, color: COLORS.secondary}}>
              <Image
                source={icons.eye}
                resizeMode="contain"
                style={{
                  width: 20,
                  height: 20,
                  tintColor: COLORS.secondary,
                }}
              />{' '}
              {item.drishti}
            </Text>
          </View>
          <View style={{flex: 0, alignItems: 'center'}}>
            <Image
              source={icons.heartBlack}
              resizeMode="contain"
              style={{
                width: 20,
                height: 20,
                tintColor: COLORS.secondary,
                paddingRight: 30,
              }}
            />
          </View>
        </View>
      </View>
    );
  }

  function renderCategoryHeader() {
    const renderItem = ({item}) => {
      return (
        <TouchableOpacity
          style={{marginRight: SIZES.padding}}
          onPress={() => setSelectedCategory(item.id)}>
          {selectedCategory == item.id && (
            <Text style={{...FONTS.h3, color: COLORS.black}}>
              {item.categoryName}
            </Text>
          )}
          {selectedCategory != item.id && (
            <Text style={{...FONTS.h4, color: COLORS.lightGray}}>
              {item.categoryName}
            </Text>
          )}
        </TouchableOpacity>
      );
    };

    return (
      <View style={{paddingLeft: SIZES.padding}}>
        <FlatList
          data={categories}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          horizontal
        />
      </View>
    );
  }

  function renderCategoryData() {
    var items = [];

    let selectedCategoryItems = categories.filter(
      a => a.id == selectedCategory,
    );

    if (selectedCategoryItems.length > 0) {
      items = selectedCategoryItems[0].items;
    }

    const renderItem = ({item}) => {
      return (
        <View style={{}}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  ...FONTS.body5,
                  color: COLORS.lightGray,
                  paddingHorizontal: SIZES.radius,
                }}>
                {item.text}
              </Text>
            </View>
          </View>
        </View>
      );
    };

    return (
      <View
        style={{flex: 1, marginTop: SIZES.radius, paddingLeft: SIZES.padding}}>
        <FlatList
          data={items}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  if (item) {
    return (
      <View style={{flex: 1, backgroundColor: COLORS.background}}>
        <View style={{height: 50}}>{renderHeader()}</View>
        <View style={{flex: 1}}>{renderAsanaInfoSection()}</View>
        <View style={{flex: 0.5}}>{renderDrishti()}</View>
        <View>{renderCategoryHeader()}</View>
        <ScrollView style={{flex: 3}}>
          <View>{renderCategoryData()}</View>
        </ScrollView>
      </View>
    );
  } else {
    return <></>;
  }
};

export default AsanaDetail;
