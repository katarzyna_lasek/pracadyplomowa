import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons} from '../constants';

const Favorites = ({navigation}) => {
  const YogaStyleData = [
    {
      id: 1,
      nameS: 'Padangusthasana',
      image: icons.yoga_1,
      drishti: 'nos',
      namePl: 'Skłon do przodu',
    },
    {
      id: 2,
      nameS: 'Vrkasana',
      image: icons.yoga_2,
      drishti: 'trzecie oko',
      namePl: 'Pozycja drzewa',
    },
    {
      id: 3,
      nameS: 'Adho Mukha Shvanasana',
      image: icons.yoga_3,
      drishti: 'nos ( nāsāgre )',
      namePl: 'Pies z głową w górę',
    },
  ];
  const [yogaStyleData] = React.useState(YogaStyleData);

  function renderHeader(title) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              {title}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderPranayamaSection(yogaStyleData) {
    const renderItem = ({item}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('AsanaDetail', {
                item: item,
              })
            }>
            <Image
              source={item.image}
              resizeMode="cover"
              style={{width: 100, height: 150, borderRadius: 10}}
            />
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.h2,
                    color: COLORS.black,
                  }}>
                  {item.nameS}
                </Text>
                <Text style={{...FONTS.h3, color: COLORS.lightGray}}>
                  {item.namePl}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: SIZES.radius}}>
                <Image
                  source={icons.eye}
                  resizeMode="contain"
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: COLORS.lightGray,
                  }}
                />
                <Text
                  style={{
                    ...FONTS.body4,
                    color: COLORS.lightGray,
                    paddingHorizontal: SIZES.radius,
                  }}>
                  {item.drishti}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{position: 'absolute', top: 60, right: 15}}
            onPress={() => console.log('dodano')}>
            <Image
              source={icons.heartBlack}
              resizeMode="contain"
              style={{
                width: 25,
                height: 25,
                tintColor: COLORS.lightGray,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, margin: SIZES.padding}}>
          <FlatList
            data={yogaStyleData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader('Ulubione Asany')}</View>
      <ScrollView>
        <View>{renderPranayamaSection(yogaStyleData)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Favorites;
