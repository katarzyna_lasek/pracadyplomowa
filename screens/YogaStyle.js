import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons, images} from '../constants';

const YogaStyle = ({navigation}) => {
  const YogaStyleData = [
    {
      id: 1,
      nameSa: 'Vinyasa yoga',
      namePl: '',
      description:
        'Vinyasa oznacza „umieszczać w specjalny sposób”, a w tym przypadku pozycje jogi. Jest często uważana za najbardziej atletyczny styl jogi, który został zaadaptowany z ashtangi jogi w latach 80.\n' +
        '\n' +
        'Jak ćwiczyć?\n' +
        'Na zajęciach vinyasy ruch jest skoordynowany z oddechem, aby przejść z jednej pozycji do drugiej. Vinyasa jest oparta na płynnym przechodzeniu między asanami, ale można po rozgrzewce pozostać w danej pozycji nieco dłużej. Style vinyasy mogą się różnić w zależności od nauczyciela i może występować wiele typów pozycji w różnych sekwencjach.',
    },
    {
      id: 2,
      nameSa: 'Hatha yoga',
      namePl: '',
      description:
        'Termin „hatha” jest terminem zbiorczym określającym wszystkie fizyczne pozycje jogi. Na Zachodzie hatha joga odnosi się do wszystkich innych stylów jogi takich jak ashtanga, Iyengar itp., które są zakorzenione w praktyce fizycznej. Joga fizyczna jest bardzo populara i ma wiele stylów.\n' +
        '\n' +
        'Jak ćwiczyć?\n' +
        'Zajęcia hatha jogi są najlepsze dla początkujących, ponieważ zazwyczaj odbywają się wolniej niż w przypadku innych stylów jogi. Zajęcia Hatha to klasyczne podejście do oddychania i ćwiczeń. Jeśli dopiero zaczynasz swoją przygodę z jogą, hatha joga jest świetnym punktem wyjścia.',
    },
    {
      id: 3,
      nameSa: 'Yoga Iyengara',
      namePl: '',
      description:
        'Joga Iyengara została założona przez B.K.S. Iyengara i koncentruje się na wyrównaniu, a także na szczegółowym i precyzyjnym ruchu. Na zajęciach Iyengara uczniowie wykonują różne pozycje, kontrolując oddech.\n' +
        '\n' +
        'Jak ćwiczyć?\n' +
        'Generalnie, pozycje są utrzymywane przez długi czas, doskonaląc szczegóły danej asany. Ten styl jogi w dużej mierze polega na używaniu pomocy (kostek, pasków), które pomagają uczniom doskonalić swoją formę i wchodzić głębiej w pozycje w bezpieczny sposób. Ten styl jest naprawdę świetny dla osób po kontuzjach lub mało rozciągniętych, które muszą pracować powoli i metodycznie.',
    },
    {
      id: 4,
      nameSa: 'Ashtanga yoga',
      namePl: '',
      description:
        'Ashtanga jest tłumaczona jako „ścieżka ośmiu kończyn”. W Mysore w Indiach ludzie zbierają się, aby wspólnie ćwiczyć tę formę jogi we własnym tempie. Vinyasa yoga wywodzi się z ashtangi jako płynnego stylu łączącego oddech z ruchem.\n' +
        '\n' +
        'Jak ćwiczyć?\n' +
        'Ashtanga joga obejmuje bardzo wymagającą fizycznie sekwencję pozycji, więc ten styl jogi zdecydowanie nie jest dla początkujących. Ashtanga zaczyna się od pięciu powitań słońca A i pięciu powitań słońca B, a następnie przechodzi do serii pozycji stojących i niskich.',
    },
    {
      id: 5,
      nameSa: 'Yin yoga',
      namePl: '',
      description:
        'Joga Yin to powolny styl jogi z pozycjami siedzącymi, które są utrzymywane przez dłuższy czas. Yin może być również medytacyjną praktyką jogi, która pomaga znaleźć wewnętrzny spokój.\n' +
        '\n' +
        'Jak ćwiczyć?\n' +
        'Yin to świetne zajęcia dla początkujących, ponieważ pozycje można utrzymywać od 45 sekund do 2 minut. Zajęcia mają charakter relaksacyjny.',
    },
  ];
  const [yogaStyleData, setYogaStyleData] = React.useState(YogaStyleData);

  function renderHeader(title) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              {title}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderButtonAsanas() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: SIZES.padding,
        }}>
        <View style={{flexDirection: 'row', height: 150}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => navigation.navigate('AsanasList')}>
            <ImageBackground
              source={images.yogaphoto1}
              resizeMode="cover"
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.secondary,
                  fontSize: 35,
                  lineHeight: 50,
                  textAlign: 'center',
                  backgroundColor: COLORS.primary,
                }}>
                Jak wybrać styl?
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderPranayamaSection(yogaStyleData) {
    const renderItem = ({item, index}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('ItemDetail', {
                item: item,
              })
            }>
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.h2,
                    color: COLORS.black,
                  }}>
                  {item.nameSa}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: SIZES.radius}}>
                <Text style={{...FONTS.h3, color: COLORS.lightGray}}>
                  {item.namePl}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, margin: SIZES.padding}}>
          <FlatList
            data={yogaStyleData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader('Style jogi')}</View>
      <ScrollView style={{marginTop: SIZES.radius}}>
        {renderButtonAsanas()}
        <View>{renderPranayamaSection(yogaStyleData)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default YogaStyle;
