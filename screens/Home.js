import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons, images} from '../constants';
import {button2} from '../data/button2.js';
import {button3} from '../data/button3.js';

const Home = ({navigation}) => {
  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
        }}>
        <View style={{flex: 1}}>
          <View style={{marginRight: SIZES.padding, marginTop: SIZES.padding}}>
            <Text style={{...FONTS.body4, color: COLORS.secondary}}>
              Asana na dziś
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Info')}>
              <Text style={{...FONTS.h1, color: COLORS.secondary}}>
                TADASANA
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity
          style={{
            backgroundColor: COLORS.background,
            marginTop: SIZES.padding,
          }}
          onPress={() => navigation.navigate('Info')}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 35,
                height: 35,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={icons.info}
                resizeMode="contain"
                style={{
                  width: 35,
                  height: 35,
                }}
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  function renderButton1() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: SIZES.padding,
        }}>
        <View style={{flexDirection: 'row', height: 150}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => navigation.navigate('AsanasList')}>
            <ImageBackground
              source={images.yogaphoto1}
              resizeMode="cover"
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.secondary,
                  fontSize: 35,
                  lineHeight: 50,
                  textAlign: 'center',
                  backgroundColor: COLORS.primary,
                }}>
                Asany
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderButtons2(button2) {
    const renderItem = ({item, index}) => {
      return (
        <TouchableOpacity
          style={{
            flex: 1,
            marginLeft: index == 0 ? SIZES.padding : 0,
            marginRight: SIZES.radius,
            width: 180,
          }}
          onPress={() => navigation.navigate(item.link)}>
          <ImageBackground
            source={item.icon}
            resizeMode="cover"
            style={{width: 170, height: 150}}>
            <Text
              style={{
                color: COLORS.secondary,
                fontSize: 20,
                lineHeight: 50,
                textAlign: 'center',
                backgroundColor: COLORS.primary,
                marginTop: SIZES.padding,
              }}>
              {item.name}
            </Text>
          </ImageBackground>
        </TouchableOpacity>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View>
          <FlatList
            data={button2}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  function renderButtons3(button3) {
    const renderItem = ({item, index}) => {
      return (
        <TouchableOpacity
          style={{
            flex: 1,
            marginLeft: index == 0 ? SIZES.padding : 0,
            marginRight: SIZES.radius,
          }}
          onPress={() => navigation.navigate(item.link)}>
          <ImageBackground
            source={item.icon}
            resizeMode="cover"
            style={{width: 112, height: 150}}>
            <Text
              style={{
                color: COLORS.secondary,
                fontSize: 20,
                lineHeight: 50,
                textAlign: 'center',
                backgroundColor: COLORS.primary,
                marginTop: SIZES.padding,
              }}>
              {item.name}
            </Text>
          </ImageBackground>
        </TouchableOpacity>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, marginTop: SIZES.padding}}>
          <FlatList
            data={button3}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            horizontal
            showsHorizontalScrollIndicator={true}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 80}}>{renderHeader()}</View>
      <ScrollView>
        <View>
          {renderButton1()}
          {renderButtons2(button2)}
          {renderButtons3(button3)}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
