import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  ImageBackground,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons, images} from '../constants';

const PranayamaList = ({navigation}) => {
  const PranayamaData = [
    {
      id: 1,
      nameSa: 'Ujjayi',
      namePl: 'intensywna | dynamiczna | medytacja w ruchu | stała sekwencja',
      description: '',
    },
    {
      id: 2,
      nameSa: 'Nadi-Sodhana',
      namePl: 'płynna | łagodna do intensywnej | różnorodne sekwencje',
      description: '',
    },
    {
      id: 3,
      nameSa: 'Kapalabhati',
      namePl:
        'precyzyjna | używa akcesoriów | długie trzymanie pozycji | wolne tempo',
      description: '',
    },
    {
      id: 4,
      nameSa: 'Viloma',
      namePl: 'medytacyjna | głównie asany siedzące | śpiewanie mantr',
      description: '',
    },
  ];
  const [pranayama, setPranayama] = React.useState(PranayamaData);

  function renderHeader(profile) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              Pranayama
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderButtonAsanas() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: SIZES.padding,
        }}>
        <View style={{flexDirection: 'row', height: 150}}>
          <TouchableOpacity
            style={{flex: 1}}
            onPress={() => navigation.navigate('ItemDetail')}>
            <ImageBackground
              source={images.yogaphoto7}
              resizeMode="cover"
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: COLORS.secondary,
                  fontSize: 35,
                  lineHeight: 50,
                  textAlign: 'center',
                  backgroundColor: COLORS.primary,
                }}>
                Jak praktykować pranayame?
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderPranayamaSection(pranayama) {
    const renderItem = ({item, index}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('ItemDetail', {
                item: item,
              })
            }>
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.h2,
                    color: COLORS.black,
                  }}>
                  {item.nameSa}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: SIZES.radius}}>
                <Text style={{...FONTS.h3, color: COLORS.lightGray}}>
                  {item.namePl}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, margin: SIZES.padding}}>
          <Text style={{...FONTS.h2}}>Techniki oddechowe:</Text>
          <FlatList
            data={pranayama}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView style={{marginTop: SIZES.radius}}>
        {renderButtonAsanas()}
        <View>{renderPranayamaSection(pranayama)}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PranayamaList;
