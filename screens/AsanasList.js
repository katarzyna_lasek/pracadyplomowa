import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';

import {COLORS, FONTS, SIZES, icons} from '../constants';

const Home = ({navigation}) => {
  const padangusthasana = {
    id: 1,
    nameS: 'Utthita Trikonasana',
    image: icons.yoga_14,
    drishti: 'nos',
    namePl: 'pozycja rozciągniętego trójkąta',
  };

  const Vrkasana = {
    id: 2,
    nameS: 'Garudasana',
    image: icons.yoga_72,
    drishti: 'trzecie oko',
    namePl: 'pozycja orła',
  };

  const adhoMukhaShvanasana = {
    id: 3,
    nameS: 'Virabhadrasana A ',
    image: icons.yoga_30,
    drishti: 'nos ( nāsāgre )',
    namePl: 'pozycja wojownika A',
  };

  const categoriesData = [
    {
      id: 1,
      categoryName: 'Stojące',
      asanas: [adhoMukhaShvanasana, padangusthasana, Vrkasana],
    },
    {
      id: 2,
      categoryName: 'Siedzące',
      asanas: [adhoMukhaShvanasana],
    },
    {
      id: 3,
      categoryName: 'Odwrócone',
      asanas: [adhoMukhaShvanasana],
    },
    {
      id: 4,
      categoryName: 'W leżenmiu',
      asanas: [adhoMukhaShvanasana],
    },
    {
      id: 5,
      categoryName: 'Medytacyjne',
      asanas: [adhoMukhaShvanasana],
    },
  ];

  const [categories, setCategories] = React.useState(categoriesData);
  const [selectedCategory, setSelectedCategory] = React.useState(1);

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              ASANY
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderCategoryHeader() {
    const renderItem = ({item}) => {
      return (
        <TouchableOpacity
          style={{flex: 1, marginRight: SIZES.padding}}
          onPress={() => setSelectedCategory(item.id)}>
          {selectedCategory == item.id && (
            <Text style={{...FONTS.h2, color: COLORS.secondary}}>
              {item.categoryName}
            </Text>
          )}
          {selectedCategory != item.id && (
            <Text style={{...FONTS.h2, color: COLORS.primary}}>
              {item.categoryName}
            </Text>
          )}
        </TouchableOpacity>
      );
    };

    return (
      <View style={{flex: 1, paddingLeft: SIZES.padding}}>
        <FlatList
          data={categories}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          horizontal
        />
      </View>
    );
  }

  function renderCategoryData() {
    var asanas = [];

    let selectedCategoryAsanas = categories.filter(
      a => a.id == selectedCategory,
    );

    if (selectedCategoryAsanas.length > 0) {
      asanas = selectedCategoryAsanas[0].asanas;
    }

    const renderItem = ({item}) => {
      return (
        <View style={{marginVertical: SIZES.base}}>
          <TouchableOpacity
            style={{flex: 1, flexDirection: 'row'}}
            onPress={() =>
              navigation.navigate('AsanaDetail', {
                item: item,
              })
            }>
            <Image
              source={item.image}
              resizeMode="cover"
              style={{width: 100, height: 150, borderRadius: 10}}
            />
            <View style={{flex: 1, marginLeft: SIZES.radius}}>
              <View>
                <Text
                  style={{
                    paddingRight: SIZES.padding,
                    ...FONTS.h2,
                    color: COLORS.black,
                  }}>
                  {item.nameS}
                </Text>
                <Text style={{...FONTS.h3, color: COLORS.lightGray}}>
                  {item.namePl}
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: SIZES.radius}}>
                <Image
                  source={icons.eye}
                  resizeMode="contain"
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: COLORS.lightGray,
                  }}
                />
                <Text
                  style={{
                    ...FONTS.body4,
                    color: COLORS.lightGray,
                    paddingHorizontal: SIZES.radius,
                  }}>
                  {item.drishti}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{position: 'absolute', top: 60, right: 15}}
            onPress={() => console.log('dodano')}>
            <Image
              source={icons.heart}
              resizeMode="contain"
              style={{
                width: 25,
                height: 25,
                tintColor: COLORS.lightGray,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View
        style={{flex: 1, marginTop: SIZES.radius, paddingLeft: SIZES.padding}}>
        <FlatList
          data={asanas}
          renderItem={renderItem}
          keyExtractor={item => `${item.id}`}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView style={{marginTop: SIZES.radius}}>
        <View>
          <View>{renderCategoryHeader()}</View>
          <View>{renderCategoryData()}</View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
