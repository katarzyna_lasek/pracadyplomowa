import React from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import {FONTS, COLORS, SIZES, icons} from '../constants';

const ItemDetail = ({route, navigation}) => {
  React.useEffect(() => {
    let {item} = route.params;
    setItem(item);
  }, [item, route.params]);

  const [item, setItem] = React.useState(null);

  function renderPlText() {
    return (
      <View style={{flex: 1, position: 'relative'}}>
        <View
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundColor: COLORS.tertiary,
            justifyContent: 'center',
          }}
        />
        <View style={{alignItems: 'center', margin: 15}}>
          <Text style={{...FONTS.h2, color: COLORS.background}}>
            {item.namePl}
          </Text>
        </View>
      </View>
    );
  }

  function renderSaText() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            marginTop: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{...FONTS.h2, color: COLORS.secondary}}>
              {item.nameSa}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderDescription() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'column',
            margin: 20,
            backgroundColor: 'rgba(0,0,0,0.3)',
            marginVertical: SIZES.base,
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{...FONTS.h3, color: COLORS.black, margin: 20, textAlign: "justify"}}>
              Następnie (tada), w rezultacie panowania nad żywiołem umysłu,
              osoba (drastuh) znajdzie się (avasthanam) w naturalnej dla siebie
              postaci (svarupa). Termin stosowany tu jako odpowiednik "osoby" to
              drastuh oznacza "tego, który widzi". Słowo to będzie stosowane w
              całych Jogasutrach w prostym, praktycznym znaczeniu "świadomej
              istoty", co podkreślać ma świadomość jako niezbędny czynnik w
              procesie samorealizacji. Derywat svarupa to bardzo interesujące
              słowo, które często tłumaczone jest błędnie. Sva znaczny po prostu
              "twój/twoja". Rdzeń ten zaobserwować można w niemal niezmienionej postaci w językach romańskich, na przykład "su" w języku hiszpańskim czy "sua" w języku portugalskim. Słowo "rupa" znaczy "postać" i często używane jest w znaczeniu "ciało"
            </Text>
          </View>
        </View>
      </View>
    );
  }

  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View
          style={{
            flexDirection: 'row',
            height: 25,
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            style={{marginLeft: SIZES.base}}
            onPress={() => navigation.goBack()}>
            <Image
              source={icons.back_arrow_icon}
              resizeMode="contain"
              style={{
                width: 25,
                height: 25,
                tintColor: COLORS.background,
              }}
            />
          </TouchableOpacity>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{...FONTS.h3, color: COLORS.secondary}} />
          </View>
        </View>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              {item.number}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  if (item) {
    return (
      <View style={{flex: 1, backgroundColor: COLORS.background}}>
        <View style={{height: 50}}>{renderHeader()}</View>
        <View style={{flex: 1}}>{renderPlText()}</View>
        <View style={{flex: 1}}>{renderSaText()}</View>
        <View style={{flex: 4}}>
          <ScrollView>
            <View>{renderDescription()}</View>
          </ScrollView>
        </View>
      </View>
    );
  } else {
    return <></>;
  }
};

export default ItemDetail;
