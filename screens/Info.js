import React from 'react';
import {SafeAreaView, View, Text, ScrollView} from 'react-native';

import {COLORS, FONTS, SIZES} from '../constants';

const Home = ({navigation}) => {
  function renderHeader() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: SIZES.padding,
          alignItems: 'center',
          backgroundColor: COLORS.primary,
        }}>
        <View style={{flex: 1}}>
          <View>
            <Text
              style={{
                ...FONTS.h2,
                color: COLORS.background,
                textAlign: 'center',
              }}>
              O APLIKACJI
            </Text>
          </View>
        </View>
      </View>
    );
  }
  function renderInfo() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flexDirection: 'column',
            paddingVertical: 5,
            backgroundColor: 'rgba(0,0,0,0.3)',
            margin: SIZES.base,
            paddingTop: 30,
            paddingBottom: 30,
            paddingLeft: 30,
            paddingRight: 30,
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{...FONTS.h2, color: COLORS.black, marginBottom: 10}}>
              Bibliografia
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              paddingHorizontal: SIZES.radius,
              alignItems: 'center',
            }}>
            <Text
              style={{
                paddingRight: SIZES.padding,
                ...FONTS.h3,
                color: COLORS.lightGray,
                marginBottom: 30,
              }}>
              Joga. Nowy ilustrowany orzewodnik anatomiczny po asanach, ruchach
              i technikakch oddychania. [L.Kaminoff, A.Matthews]{'\n'}
              Anatomia Hatha Jogi [H.D.Coulter]{'\n'}
              Jogasutry Patandżalego [G.Dauster]{'\n'}
              Uważność w praktyce [M.Wielobób]{'\n'}
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{...FONTS.h2, color: COLORS.black, marginBottom: 10}}>
              Grafika
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text
              style={{
                paddingRight: SIZES.padding,
                ...FONTS.h3,
                color: COLORS.lightGray,
              }}>
              flaticon.com -> Freepik, Stockio, Gregor Cresnar, Roundicons,com
              {'\n'}
              unsplash.com -> Carl Barcelo, Rawan Yasser
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.background}}>
      <View style={{height: 50}}>{renderHeader()}</View>
      <ScrollView>
        <View>{renderInfo()}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
