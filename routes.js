const express = require('express');
const mysql = require('mysql');

const connection = mysql.createPool({
  host: '',
  user: '',
  password: '',
  database: '',
});

const app = express();
const title = [
  'asanacategory',
  'asanas',
  'ashtanga',
  'mantras',
  'pranayama',
  'sanskritnumbers',
  'yogasutras',
  'yogatype',
];

for (let i = 0; i < title.length; i++) {
  initRest(title[i]);
}

function initRest(endpoint) {
  app.get('/' + `${endpoint}`, function (req, res) {
    connection.getConnection(function (err, connection) {
      connection.query(
        'SELECT * FROM ' + `${endpoint}`,
        function (error, results, fields) {
          if (error) {
            throw error;
          }
          res.send(results);
        },
      );
    });
  });
}

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});

app.listen(3000, () => {
  console.log('Go to http://localhost:3000/asanas so you can see the data.');
});
