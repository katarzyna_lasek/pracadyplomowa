import React from 'react';
import {Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Home} from '../screens/';
import {PranayamaList} from '../screens/';
import {AsanasList} from '../screens';
import {AsanaDetail} from '../screens';
import {Mantra} from '../screens';
import {Favorites} from '../screens';
import {Jogasutry} from '../screens';
import {YogaLevel} from '../screens';
import {YogaStyle} from '../screens';
import {Counting} from '../screens';
import {Info} from '../screens';
import {ItemDetail} from '../screens';
import {icons, COLORS} from '../constants';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

const Tab = createBottomTabNavigator();

const tabOptions = {
  showLabel: false,
  style: {
    height: '9%',
    backgroundColor: COLORS.menu,
  },
};

const SettingsStack = createNativeStackNavigator();

function SettingsStackScreen() {
  return (
    <SettingsStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Mantra" component={Mantra} />
      <Tab.Screen name="Jogasutry" component={Jogasutry} />
      <Tab.Screen name="Counting" component={Counting} />
      <Tab.Screen name="YogaLevel" component={YogaLevel} />
      <Tab.Screen name="YogaStyle" component={YogaStyle} />
      <Tab.Screen name="Info" component={Info} />
      <Tab.Screen name="ItemDetail" component={ItemDetail} />
    </SettingsStack.Navigator>
  );
}

function Asana() {
  return (
    <SettingsStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name="AsanasList" component={AsanasList} />
      <Tab.Screen name="AsanaDetail" component={AsanaDetail} />
    </SettingsStack.Navigator>
  );
}

const Tabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={tabOptions}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          const tintColor = focused ? COLORS.primary : COLORS.secondary;

          switch (route.name) {
            case 'Home':
              return (
                <Image
                  source={icons.dashboard_icon}
                  resizeMode="contain"
                  style={{
                    tintColor: tintColor,
                    width: 25,
                    height: 25,
                  }}
                />
              );

            case 'PranayamaList':
              return (
                <Image
                  source={icons.om}
                  resizeMode="contain"
                  style={{
                    tintColor: tintColor,
                    width: 30,
                    height: 30,
                  }}
                />
              );

            case 'Asana':
              return (
                <Image
                  source={icons.bandha}
                  resizeMode="contain"
                  style={{
                    tintColor: tintColor,
                    width: 25,
                    height: 25,
                  }}
                />
              );

            case 'Favorites':
              return (
                <Image
                  source={icons.heartBlack}
                  resizeMode="contain"
                  style={{
                    tintColor: tintColor,
                    width: 25,
                    height: 25,
                  }}
                />
              );
          }
        },
      })}>
      <Tab.Screen name="Home" component={SettingsStackScreen} />
      <Tab.Screen name="Asana" component={Asana} />
      <Tab.Screen name="PranayamaList" component={PranayamaList} />
      <Tab.Screen name="Favorites" component={Favorites} />
    </Tab.Navigator>
  );
};

export default Tabs;
