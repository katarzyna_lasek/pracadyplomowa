export const yogaphoto1 = require('../assets/images/yogaphoto1.jpg');
export const yogaphoto2 = require('../assets/images/yogaphoto2.jpg');
export const yogaphoto3 = require('../assets/images/yogaphoto3.jpg');
export const yogaphoto4 = require('../assets/images/yogaphoto4.jpg');
export const yogaphoto5 = require('../assets/images/yogaphoto5.jpg');
export const yogaphoto6 = require('../assets/images/yogaphoto6.jpg');
export const yogaphoto7 = require('../assets/images/yogaphoto7.jpg');
export const yogaphoto8 = require('../assets/images/yogaphoto8.jpg');
export const yogaphoto9 = require('../assets/images/yogaphoto9.jpg');

export default {
  yogaphoto1,
  yogaphoto2,
  yogaphoto3,
  yogaphoto4,
  yogaphoto5,
  yogaphoto6,
  yogaphoto7,
  yogaphoto8,
  yogaphoto9,
};
