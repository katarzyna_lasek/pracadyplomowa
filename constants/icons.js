export const dashboard_icon = require('../assets/icons/dashboard_icon.png');
export const back_arrow_icon = require('../assets/icons/back_arrow_icon.png');
export const om = require('../assets/icons/om.png');
export const bandha = require('../assets/icons/bandha.png');
export const info = require('../assets/icons/info.png');
export const heart = require('../assets/icons/heart.png');
export const heartBlack = require('../assets/icons/heartBlack.png');
export const eye = require('../assets/icons/eye.png');

export const yoga = require('../assets/icons/yoga.png');
export const yoga_1 = require('../assets/icons/yoga-1.png');
export const yoga_2 = require('../assets/icons/yoga-2.png');
export const yoga_3 = require('../assets/icons/yoga-3.png');
export const yoga_4 = require('../assets/icons/yoga-4.png');
export const yoga_5 = require('../assets/icons/yoga-5.png');
export const yoga_6 = require('../assets/icons/yoga-6.png');
export const yoga_7 = require('../assets/icons/yoga-7.png');
export const yoga_8 = require('../assets/icons/yoga-8.png');
export const yoga_9 = require('../assets/icons/yoga-9.png');
export const yoga_10 = require('../assets/icons/yoga-10.png');
export const yoga_11 = require('../assets/icons/yoga-11.png');
export const yoga_12 = require('../assets/icons/yoga-12.png');
export const yoga_13 = require('../assets/icons/yoga-13.png');
export const yoga_14 = require('../assets/icons/yoga-14.png');
export const yoga_15 = require('../assets/icons/yoga-15.png');
export const yoga_16 = require('../assets/icons/yoga-16.png');
export const yoga_17 = require('../assets/icons/yoga-17.png');
export const yoga_18 = require('../assets/icons/yoga-18.png');
export const yoga_19 = require('../assets/icons/yoga-19.png');
export const yoga_20 = require('../assets/icons/yoga-20.png');
export const yoga_21 = require('../assets/icons/yoga-21.png');
export const yoga_22 = require('../assets/icons/yoga-22.png');
export const yoga_23 = require('../assets/icons/yoga-23.png');
export const yoga_24 = require('../assets/icons/yoga-24.png');
export const yoga_25 = require('../assets/icons/yoga-25.png');
export const yoga_26 = require('../assets/icons/yoga-26.png');
export const yoga_27 = require('../assets/icons/yoga-27.png');
export const yoga_28 = require('../assets/icons/yoga-28.png');
export const yoga_29 = require('../assets/icons/yoga-29.png');
export const yoga_30 = require('../assets/icons/yoga-30.png');
export const yoga_31 = require('../assets/icons/yoga-31.png');
export const yoga_32 = require('../assets/icons/yoga-32.png');
export const yoga_33 = require('../assets/icons/yoga-33.png');
export const yoga_34 = require('../assets/icons/yoga-34.png');
export const yoga_35 = require('../assets/icons/yoga-35.png');
export const yoga_36 = require('../assets/icons/yoga-36.png');
export const yoga_37 = require('../assets/icons/yoga-37.png');
export const yoga_38 = require('../assets/icons/yoga-38.png');
export const yoga_39 = require('../assets/icons/yoga-39.png');
export const yoga_40 = require('../assets/icons/yoga-40.png');
export const yoga_41 = require('../assets/icons/yoga-41.png');
export const yoga_42 = require('../assets/icons/yoga-42.png');
export const yoga_43 = require('../assets/icons/yoga-43.png');
export const yoga_44 = require('../assets/icons/yoga-44.png');
export const yoga_45 = require('../assets/icons/yoga-45.png');
export const yoga_46 = require('../assets/icons/yoga-46.png');
export const yoga_47 = require('../assets/icons/yoga-47.png');
export const yoga_48 = require('../assets/icons/yoga-48.png');
export const yoga_49 = require('../assets/icons/yoga-49.png');
export const yoga_50 = require('../assets/icons/yoga-50.png');
export const yoga_51 = require('../assets/icons/yoga-51.png');
export const yoga_52 = require('../assets/icons/yoga-52.png');
export const yoga_53 = require('../assets/icons/yoga-53.png');
export const yoga_54 = require('../assets/icons/yoga-54.png');
export const yoga_55 = require('../assets/icons/yoga-55.png');
export const yoga_56 = require('../assets/icons/yoga-56.png');
export const yoga_57 = require('../assets/icons/yoga-57.png');
export const yoga_58 = require('../assets/icons/yoga-58.png');
export const yoga_59 = require('../assets/icons/yoga-59.png');
export const yoga_60 = require('../assets/icons/yoga-60.png');
export const yoga_61 = require('../assets/icons/yoga-61.png');
export const yoga_62 = require('../assets/icons/yoga-62.png');
export const yoga_63 = require('../assets/icons/yoga-63.png');
export const yoga_64 = require('../assets/icons/yoga-64.png');
export const yoga_65 = require('../assets/icons/yoga-65.png');
export const yoga_66 = require('../assets/icons/yoga-66.png');
export const yoga_67 = require('../assets/icons/yoga-67.png');
export const yoga_68 = require('../assets/icons/yoga-68.png');
export const yoga_69 = require('../assets/icons/yoga-69.png');
export const yoga_70 = require('../assets/icons/yoga-70.png');
export const yoga_71 = require('../assets/icons/yoga-71.png');
export const yoga_72 = require('../assets/icons/yoga-72.png');
export const yoga_73 = require('../assets/icons/yoga-73.png');
export const yoga_74 = require('../assets/icons/yoga-74.png');
export const yoga_75 = require('../assets/icons/yoga-75.png');
export const yoga_76 = require('../assets/icons/yoga-76.png');
export const yoga_77 = require('../assets/icons/yoga-77.png');
export const yoga_78 = require('../assets/icons/yoga-78.png');
export const yoga_79 = require('../assets/icons/yoga-79.png');
export const yoga_80 = require('../assets/icons/yoga-80.png');
export const yoga_81 = require('../assets/icons/yoga-81.png');
export const yoga_82 = require('../assets/icons/yoga-82.png');
export const yoga_83 = require('../assets/icons/yoga-83.png');
export const yoga_84 = require('../assets/icons/yoga-84.png');
export const yoga_85 = require('../assets/icons/yoga-85.png');
export const yoga_86 = require('../assets/icons/yoga-86.png');
export const yoga_87 = require('../assets/icons/yoga-87.png');
export const yoga_88 = require('../assets/icons/yoga-88.png');
export const yoga_89 = require('../assets/icons/yoga-89.png');
export const yoga_90 = require('../assets/icons/yoga-90.png');
export const yoga_91 = require('../assets/icons/yoga-91.png');
export const yoga_92 = require('../assets/icons/yoga-92.png');
export const yoga_93 = require('../assets/icons/yoga-93.png');
export const yoga_94 = require('../assets/icons/yoga-94.png');
export const yoga_95 = require('../assets/icons/yoga-95.png');
export const yoga_96 = require('../assets/icons/yoga-96.png');
export const yoga_97 = require('../assets/icons/yoga-97.png');
export const yoga_98 = require('../assets/icons/yoga-98.png');
export const yoga_99 = require('../assets/icons/yoga-99.png');

export default {
  dashboard_icon,
  back_arrow_icon,
  om,
  bandha,
  info,
  heart,
  heartBlack,
  eye,
  yoga,
  yoga_1,
  yoga_2,
  yoga_3,
  yoga_4,
  yoga_5,
  yoga_6,
  yoga_7,
  yoga_8,
  yoga_9,
  yoga_10,
  yoga_11,
  yoga_12,
  yoga_13,
  yoga_14,
  yoga_15,
  yoga_16,
  yoga_17,
  yoga_18,
  yoga_19,
  yoga_20,
  yoga_21,
  yoga_22,
  yoga_23,
  yoga_24,
  yoga_25,
  yoga_26,
  yoga_27,
  yoga_28,
  yoga_29,
  yoga_30,
  yoga_31,
  yoga_32,
  yoga_33,
  yoga_34,
  yoga_35,
  yoga_36,
  yoga_37,
  yoga_38,
  yoga_39,
  yoga_40,
  yoga_41,
  yoga_42,
  yoga_43,
  yoga_44,
  yoga_45,
  yoga_46,
  yoga_47,
  yoga_48,
  yoga_49,
  yoga_50,
  yoga_51,
  yoga_52,
  yoga_53,
  yoga_54,
  yoga_55,
  yoga_56,
  yoga_57,
  yoga_58,
  yoga_59,
  yoga_60,
  yoga_61,
  yoga_62,
  yoga_63,
  yoga_64,
  yoga_65,
  yoga_66,
  yoga_67,
  yoga_68,
  yoga_69,
  yoga_70,
  yoga_71,
  yoga_72,
  yoga_73,
  yoga_74,
  yoga_75,
  yoga_76,
  yoga_77,
  yoga_78,
  yoga_79,
  yoga_80,
  yoga_81,
  yoga_82,
  yoga_83,
  yoga_84,
  yoga_85,
  yoga_86,
  yoga_87,
  yoga_88,
  yoga_89,
  yoga_90,
  yoga_91,
  yoga_92,
  yoga_93,
  yoga_94,
  yoga_95,
  yoga_96,
  yoga_97,
  yoga_98,
  yoga_99,
};
