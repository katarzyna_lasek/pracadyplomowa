import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';

import {AsanaDetail} from './screens/';
import {Mantra} from './screens/';
import {Jogasutry} from './screens/';
import {Counting} from './screens/';
import {YogaLevel} from './screens/';
import {YogaStyle} from './screens/';
import {Info} from './screens/';
import Tabs from './navigation/tabs';
import {ItemDetail} from './screens/';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    border: 'transparent',
  },
};

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer theme={theme}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName={'Home'}>
        <Stack.Screen name="Home" component={Tabs} />
        <Stack.Screen
          name="AsanaDetail"
          component={AsanaDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Jogasutry"
          component={Jogasutry}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ItemDetail"
          component={ItemDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Mantra"
          component={Mantra}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Counting"
          component={Counting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="YogaLevel"
          component={YogaLevel}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="YogaStyle"
          component={YogaStyle}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Info"
          component={Info}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
